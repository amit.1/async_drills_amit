function fetchRandomNumbers() {
    return new Promise((resolve, reject) => {
        console.log('Fetching number...');
        setTimeout(() => {
            let randomNum = Math.floor(Math.random() * (100 - 0 + 1)) + 0;
            console.log('Received random number:', randomNum);
            resolve(randomNum);
        }, (Math.floor(Math.random() * (5)) + 1) * 1000);
    })
}

function fetchRandomString() {
    return new Promise((resolve, reject) => {
        console.log('Fetching string...');
        setTimeout(() => {
            let result = '';
            let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            let charactersLength = characters.length;
            for (let i = 0; i < 5; i++) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            console.log('Received random string:', result);
            resolve(result);
        }, (Math.floor(Math.random() * (5)) + 1) * 1000);
    })
}


function add(val_1, val_2) {
    return val_1 + val_2;
}


// ASync Await Drill.
//*************Task-1****************
// async function toFetchRandomNumber()
// {
//     let promise= await fetchRandomNumbers();
//     if(promise)
//     {
//         console.log(promise);
//     }
// }
// toFetchRandomNumber();

// async function toFetchRandomString()
// {
//     let promise=await fetchRandomString();
//     if(promise)
//     {
//         console.log(promise);
//     }
// }
//  toFetchRandomString();

// *********Task-2***********//
// async function toFetchRandomNumber()
// {
//     let promise1=await fetchRandomNumbers();
//     let sum=add(0,promise1);
//     console.log('Promise1 Value is=',promise1);
//     console.log('Sum is=',sum);
//     let promise2=await fetchRandomNumbers();
//     console.log('Promise2 Value is=',promise2)
//     console.log('Sum is=',add(sum,promise2));
// }
// toFetchRandomNumber();

//************Task-3*************** */
// async function concatenateNumberAndString()
// {
//     let number=await fetchRandomNumbers();
//     let string=await fetchRandomString();
//     console.log('Concatenated String is ',number+string);
// }
// concatenateNumberAndString();

// **********TASK-4**************
async function sumOfNumbers()
{
    let numberArray=[];
    for(let i=0;i<10;i++)
    {
        numberArray.push(await fetchRandomNumbers());
    }
    let sum=numberArray.reduce((acc,currentValue)=> acc+currentValue , 0)
    console.log(numberArray);
    console.log('sum is',sum)
}
sumOfNumbers();






// ********Task-4***************
// var promiseArray=[];
// for(let i=0;i<10;i++)
// {   
//     promiseArray.push(fetchRandomNumbers());
// }
// Promise.all(promiseArray)
// .then((Response)=>
// {
//     let sum=Response.reduce((acc,currentValue)=> acc+currentValue,0);
//     console.log(Response);
//     console.log('Sum=',sum);
// })



//***************** */  Task-3***********************************
// Promise.all([fetchRandomNumbers(),fetchRandomString()])
// .then(response=>
//     {
//         let concatString=response[0]+response[1];
//         console.log('Concatenated String is',concatString);
//     })



// ****************Task-2************************
// fetchRandomNumbers()
//     .then((response) => {
//         var sum = add(0, response);
//         console.log(sum);
//         fetchRandomNumbers()
//             .then((response) => {
//                 console.log(add(sum, response));
//             })
//     })

// *************Task 2 Completed****************

